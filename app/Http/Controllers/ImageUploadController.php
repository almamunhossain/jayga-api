<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageUploadController extends Controller
{

    //get images
    public function getImages(Request $request)
    {
        // Get user_id and vendor_id from query parameters, if provided
        $user_id = $request->query('user_id');
        $vendor_id = $request->query('vendor_id');

        // Build the query to filter images based on user_id and vendor_id
        $query = Image::query();

        if ($user_id) {
            $query->where('user_id', $user_id);
        }

        if ($vendor_id) {
            $query->where('vendor_id', $vendor_id);
        }

        // Retrieve the filtered images
        $images = $query->get();

        // You can return the images as a JSON response or as needed
        return response()->json($images);
    }

    //upload images to the public directory
    public function uploadImages(Request $request)
    {
        // Validate the incoming request
        $request->validate([
            'user_id' => 'required|integer',
            'vendor_id' => 'required|integer',
            'images.*' => 'required|image|mimes:jpeg,png,gif|max:2048', // Adjust validation rules as needed
        ]);

        //files as an empty array
        $files = [];

        //loop to the files and store them into the files array
        if ($request->file('images')) {
            foreach ($request->file('images') as $key => $file) {
                $fileName = time() . rand(1, 99) . '.' . $file->extension();
                $file->move(public_path('uploads'), $fileName);

                // Set the user_id and vendor_id for each file
                $fileData = [
                    'user_id' => $request->input('user_id'),
                    'vendor_id' => $request->input('vendor_id'),
                    'name' => $fileName,
                ];

                $files[] = $fileData;
            }
        }

        //store each files to database
        foreach ($files as $key => $file) {
            Image::create($file);
        }
        return response()->json(["message" => "Image uploaded successfully", "data" => $files]);
    }
}
